package handlers;

import common.Purchase;
import common.Type;

/**
 * //TODO - If needed, validate logic and if possible optimize code.
 */
public class Director extends Approver {

    @Override
    public void approve(Purchase purchase) {
        if (canApprove(purchase)) {
            System.out.println("Director approved purchase with id " + purchase.getId() + " that costs " + purchase.getCost());
            return;
        }

        System.out.println("Purchase with id " + purchase.getId() + " needs approval from higher position than Director.");
        next.approve(purchase);
    }

    @Override
    protected boolean canApprove(Purchase purchase) {
        if (purchase.getType() == Type.CONSUMABLES && purchase.getCost() <= 500) {
            return true;
        } else if (purchase.getType() == Type.CLERICAL && purchase.getCost() <= 1000) {
            return true;
        } else if (purchase.getType() == Type.GADGETS && purchase.getCost() <= 1500) {
            return true;
        } else if (purchase.getType() == Type.GAMING && purchase.getCost() <= 3500) {
            return true;
        } else return purchase.getType() == Type.PC && purchase.getCost() <= 6000;
    }
    }

