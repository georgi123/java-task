package handlers;

import common.Purchase;
import common.Type;

/**
 * //TODO - If needed, validate logic and if possible optimize code
 */
public class Manager extends Approver {
    @Override
    public void approve(Purchase purchase) {
        if (canApprove(purchase)) {
            System.out.println("Manager approved purchase with id " + purchase.getId() + " that costs " + purchase.getCost());
            return;
        }

        System.out.println("Purchase with id " + purchase.getCost() + " needs approval from higher position than Manager.");
        next.approve(purchase);
    }

    @Override
    protected boolean canApprove(Purchase purchase) {
        boolean result = false;

          if (purchase.getType() == Type.CONSUMABLES && purchase.getCost() <= 300) {
            return true;
        } else if (purchase.getType() == Type.CLERICAL && purchase.getCost() <= 500) {
            return true;
        } else if (purchase.getType() == Type.GADGETS && purchase.getCost() <= 1000) {
            return true;
        } else if (purchase.getType() == Type.GAMING && purchase.getCost() <= 3000) {
            return true;
        } else return purchase.getType() == Type.PC && purchase.getCost() <= 5000;
    }
    }

