import common.Purchase;
import common.Type;
import handlers.Approver;
import handlers.Manager;

/**
 * Execution class of the application.
 * Be free to modify below line 14 (bellow comment line)
 */
public class PurchaseApprovalExecutor {

    public static void execute() {
        Approver manager = new Manager();
        ApprovalChainGenerator.generate(manager);
        //Be free to modify method below this line

        manager.approve(new Purchase(1, 15000, Type.CONSUMABLES));
        manager.approve(new Purchase(2, 5000, Type.PC));
        manager.approve(new Purchase(3, 5000, Type.PC));
        manager.approve(new Purchase(4, 3000, Type.CLERICAL));
    }
}
